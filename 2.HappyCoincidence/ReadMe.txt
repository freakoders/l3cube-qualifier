HappyCoincidence™ is a program that verifies the Birthday Paradox, using Celebrity Birthdays from famousbirthdays.com/.

The program makes groups of varying numbers of random celebrities and finds fraction of groups containing common birthdays.
A graph of group size vs percentage of groups containing birthday repeats is plotted and compared with an actual plot using mathematical predictions.

Output is a list of actual vs predicted values, followed by a graphical plot.

HappyCoincidence was developed using Python 2.7.6.(in Linux)

REQUIRED LIBRARIES:
numpy
matplotlib
BeautifulSoup(bs4)

USAGE:
Program consists of three files- database, birthday.py and birthdayprocess.py

>>python birthday.py
(To fetch database.Requires an active internet connection)
**NOTE: 'database' will be altered after using birthday.py if execution is not completed, use the 'database-Copy' for furthur processing. 

>>python birthdayprocess.py
(Get graph)