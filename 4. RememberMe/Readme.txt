RememberMe™ is an application that attempts to implement simple version control on a text file. 
Only two simple operations are possible on the file:
1. A new line is appended.
2. An existing line is deleted.
After each operation, the file is committed and a new version created.

RememberMe was developed for and tested in Python 2.7.6 (in Linux)
NOTE : Because of apparent differences in file handling in windows, we recommend running the program in Linux.

Input is a changed text file to commit a new version, or a filename and a version number (n) to display the nth version of the file.

USAGE:
Input is given via command line arguments.
Two possible inputs exist:
1. One argument is given (filename). In this case a new version of the file is committed if valid.
	>> python svc.py <filename>
    eg. >> python svc.py test.txt
2. Two arguments are passed (filename and version number). In this case, the appropriate version of the file will be displayed.
	>> python svc.py <filename> <version number>
    eg. >> python svc.py test.txt 3