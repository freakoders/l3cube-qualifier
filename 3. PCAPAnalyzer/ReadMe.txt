PCAPAnalyzer is a program to read a .pcap file with ARP requests/responses('arp.pcap') and extract information and convert to a readable form.('arpinfo.txt').

The program was developed in Python 2.7.6

Consists of three files- arp.pcap(input file), pcap.py(program) and arpinfo.txt(output file)
 
USAGE:
>>>python pcap.py
 