'''

All men must Code

'''

def check(i,data):
    temp=i
    if (hex(ord(data[temp]))=="0x3c" and hex(ord(data[temp+1]))=="0x0" and hex(ord(data[temp+2]))=="0x0" and hex(ord(data[temp+3]))=="0x0" and hex(ord(data[temp+4]))=="0x3c" and hex(ord(data[temp+5]))=="0x0" and hex(ord(data[temp+6]))=="0x0" and hex(ord(data[temp+7]))=="0x0" ):  
        #print i,"Hello!"
        return i+8
    else:
        return -1

def process(i,data):
    packet=[]
    for j in range(i,i+60):
        temp=hex(ord(data[j]))
        packet.append(temp)
    return packet

def printMAC(addr):
    temp=""
    for i in range(0,5):
        temp+=addr[i][2:]+":"
    temp+= addr[5][2:]
    return temp
    


def printIP(addr):
    temp=""
    for i in range(0,3):
        temp+=str(int(addr[i],base=16))+"."
    temp+=str(int(addr[3],base=16))
    return temp

def printpadding(padding):
    temp=""
    for ch in padding:
        temp+=ch[2:]
    return temp

def printpacket(listofpackets):
    fp=open("arpinfo.txt",'w')
    i=1
    for packet in listofpackets:
        fp.write( "*********************************************************************************\n")
        fp.write("Packet "+str(i)+"\n")
        fp.write( "Ethernet\n")
        fp.write( "\tDestination Address: "+printMAC(packet[0:6])+"\n")
        fp.write( "\tSource Address: "+printMAC(packet[6:12])+"\n")
        fp.write( "\tType: "+packet[12][2:]+" "+packet[13][2:]+"\n")
        fp.write( "\tPadding: "+printpadding(packet[42:60])+"\n")
        fp.write( "ARP\n")
        fp.write( "\tHardware Type: "+packet[14][2:]+packet[15][2:]+"\n")
        fp.write( "\tProtocol Type: "+packet[16][2:]+packet[17][2:]+"\n")
        fp.write( "\tHardware Size: "+str(int(packet[18],base=16))+"\n")
        fp.write( "\tProtocol Size: "+str(int(packet[19],base=16))+"\n")
        fp.write( "\tOpcode: "+str(int(packet[20],base=16))+str(int(packet[21],base=16))+"\n")
        fp.write( "\tSender MAC: "+printMAC(packet[22:28])+"\n")
        fp.write( "\tSender IP: "+printIP(packet[28:32])+"\n")
        fp.write( "\tTarget MAC: "+printMAC(packet[32:38])+"\n")
        fp.write( "\tTarget IP: "+printIP(packet[38:42])+"\n")
        i+=1
        
def main():
    handle=open("arp.pcap",'rb')
    data=handle.read()
    listofpackets=[]
    i=0
    while i<len(data):
        ch=data[i]
        x=hex(ord(ch))
        if(x=="0x3c"):
            #print i,"animan"
            start=check(i,data)
            if(start != -1):
                #print "world!"
                temppack=process(start,data)
                listofpackets.append(temppack)
                i+=60
            else:
                i+=1
        else:
            i+=1

    #print len(listofpackets)
    printpacket(listofpackets)

main()

#FreakEnd
