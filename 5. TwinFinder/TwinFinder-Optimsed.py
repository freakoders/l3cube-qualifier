'''

All men must Code

'''

import os
import hashlib
import subprocess
import sys
import threading
import time

listOfDrives=[]
listOfFilePaths=[]
listOfFiles=[]
listOfDuplicates=[]

ADDir=0
ADFile=0
DupFound=0
DupDeleted=0

class fileInfo:
    name=""
    hashvalue=""

class filepath:
    name=""
    
#Recursive DFS to traverse through the 'directory'
def findall(directory):
    try:
        files=os.listdir(directory)
    except:
        #print "Access denied!\t",directory
        global ADDir
        ADDir=ADDir+1
        return -1
    for fl in files:
        path=os.path.join(directory,fl)
        if os.path.isdir(path):
            print path
            findall(path)
        else:
            #print path
            #dohashing(path)
            temppath=fileInfo()
            temppath.name=path
            listOfFiles.append(temppath)
    return

#Store file path & md5 hash in listOfFiles
def dohashing(fileobj,i):
    path=fileobj.name
    try:
        fileHandle=open(path,'rb')
        content=fileHandle.read()
        m=hashlib.md5(content)
        myobject=fileInfo()
        myobject.name=path
        myobject.hashvalue=m.hexdigest()
        listOfFiles[i]=myobject
    except:
        #print "Access Denied!",tempfile.name
        global ADFile
        ADFile=ADFile+1

#Find hashvalues of ALL
def allhashing():
    i=0;
    for fileobj in listOfFiles:
        t=threading.Thread(target=dohashing,args=(fileobj,i,))
        t.daemon=True
        t.run()
        i+=1
        


#MAIN STARTS HERE

startTime=time.time()        
argc=len(sys.argv)
if(argc==2):
    givenpath=sys.argv[1]
    print "Searching in ",givenpath,"...."
    res=findall(givenpath)
    if(res==-1):
        print "Invalid path/directory!"
        exit()
    else:
        print "Finished searching."
    
elif(argc==1):
    #Getting list of drives in driveLines
    drivelist = subprocess.Popen('wmic logicaldisk get name', shell=True, stdout=subprocess.PIPE)
    drivelisto, err = drivelist.communicate()
    driveLines = drivelisto.split('\n')

    #Usable drives in listOfDrives
    for d in driveLines:
        #print "YO!"
        x=str(d)
        if x.find(":")!=-1:
            listOfDrives.append(x)

            
    #Making a HUGE list of hashed files along with path
    for drive in listOfDrives:
        print "Searching in ",drive,"..."
        drive=drive.strip()+'/'
        findall(drive)
        print "Finished searching ",drive
        #for l in listOfFiles:
            #print l.name,"\t",l.hashvalue

else:
    print "Invalid arguements!"
    exit()

#Find md5 hashes of all files
print "Processing..."
allhashing()

#print "\n*************AFTER SORTING******************\n"
#Sort the listOfFiles
listOfFiles.sort(key=lambda fileInfo : fileInfo.hashvalue,reverse=True)
#for l in listOfFiles:
    #print l.hashvalue


#Accumulate duplicates together
#global DupFound
templist=[]
templist.append(listOfFiles[0])
x=listOfFiles[0].hashvalue
for i in range(1,len(listOfFiles)):
    y=listOfFiles[i].hashvalue
    if x==y:
        templist.append(listOfFiles[i])
        DupFound=DupFound+1
        #print len(templist)
        #for a in templist:
            #print a.name
    else:
        if len(templist)>1:
            listOfDuplicates.append(templist)
        templist=[]
        templist.append(listOfFiles[i])
        x=listOfFiles[i].hashvalue

#if duplicates at end of list
if len(templist)>1:
    listOfDuplicates.append(templist)

#Time required
print time.time()-startTime,"seconds"

#global DupDeleted                
for dupList in listOfDuplicates:
    print "--------------------------------------------------"
    for eachFile in dupList:
        print eachFile.name
    print "--------------------------------------------------Delete all copies?(Y/N) : Press S to finish deleting and view stats"
    x=str(raw_input())
    if x=='S' or x=='s':
        break
    elif x=='y' or x== 'Y':
        for i in range(1,len(dupList)):
            try:
                os.remove(dupList[i].name)
                DupDeleted=DupDeleted+1
            except:
                print "Error! Could not delete ",dupList[i].name
        print "Unique copy at ",dupList[0].name

print "***** STATS *****"
print "Access denied to ",ADDir," directories"
print "Access denied to ",ADFile," files"
print "Duplicates found : ",DupFound
print "Duplicates deleted : ",DupDeleted


#FreakEnd
