TwinFinder™ is a simple application to scan drives in Windows and list duplicate files.

USAGE:
Command line arguements can be passed to the program
Program with no arguements scans all the drives on Windows(External storages included)
Program can also be provided with an optional arguement specifying the directory to search in.
eg:-TwinFinder.py 							"scans all drives
	TwinFinder.py C:\Program Files			"scans all directories in C:\Program Files
	
After finding the duplicates, program can be used to delete ALL the duplicate copies of the file and keep one copy.
To skip to the statistics press 'S'

Statistics show the number of duplicates found and deleted.
It also shows the files and directories to which access was denied.

Program was developed using Python 2.7.6
Program was tested on Windows 7 and Windows 8. 
(750GB hard disk with 75GB data + 1TB external hard disk with 300GB data scan took about an hour on a machine with 6GB RAM)
(Find the attached log file of the complete scan)

UPDATE:
For the optimized version
>>>TwinFinder-Optimised.py

>>>TwinFinder-Optimised.py C:\Program Files

After optimizing, we got a 10% improvement in execution time. 
