'''

All men must Code

'''

import log_entry_module
import matplotlib.pyplot as plt
import numpy as np
import sys

arglen=len(sys.argv)


if arglen!=2:
	print "Invalid number of arguments"
	exit(0)
	
open_file_name=sys.argv[1]
text_file=open(open_file_name,"r")
buff=text_file.read()
buff_split=buff.split("\n")

log_entry_list=[]
temp_list=[]

for i in buff_split[:-1]:
	entry_split=i.split(" ")
	del temp_list[:]
	temp_list.append(entry_split[0])
	temp_list.append(entry_split[1])
	temp_list.append(entry_split[2])
	temp_date=entry_split[3].split(":")
	temp_list.append(temp_date[0][1:])
	temp_time= temp_date[1]+":"+temp_date[2]+":"+temp_date[3]
	temp_list.append(temp_time)
	temp_list.append(entry_split[4][:-1])
	
	
	temp_request=""
	count=5
	for y in entry_split[5:]:
		temp_request=temp_request+y+" "
		count=count+1
		if y[len(y)-1]=='"':
			break
	temp_list.append(temp_request[:-1])
	
	temp_list.append(entry_split[count])
	count=count+1
	temp_list.append(entry_split[count])
	count=count+1
	
	
	
	temp_referer=""
	for y in entry_split[count:]:
		temp_referer=temp_referer+y+" "
		count=count+1
		if y[len(y)-1]=='"':
			break
	temp_list.append(temp_referer[:-1])
	
	temp_user_agent=""
	for y in entry_split[count:]:
		temp_user_agent=temp_user_agent+y+" "
		count=count+1
		
		if len(y)==0 or y[len(y)-1]=='"':
			break
	temp_list.append(temp_user_agent[:-1])
	
	temp_entry=log_entry_module.log_entry()
	temp_entry.set_all(temp_list)
	log_entry_list.append(temp_entry)
	
req_rep=open("request_report.txt",'w')
req_rep.write("{:<16}{:<12}{:<9}{:<10}{:<10}{:<50}\n".format("remote_host","date","time","status","bytes","request"))
req_rep.write("________________________________________________________________________________________________________________________________________\n")
for i in log_entry_list:

	req_rep.write("{:<16}{:<12}{:<9}{:<10}{:<10}{:<50}\n".format(i.remote_host,i.date,i.time,i.status,i.bytes,i.request))
	#print i.remote_host,
	#print i.rfc931,
	#print i.authuser,
	#print i.date,
	#print i.time,
	#print i.timezone,
	#print i.request,
	#print i.status,
	#print i.bytes,
	#print i.referer,
	#print i.user_agent
	

#option=int(raw_input("Enter option: "))
option=1
if option==1:
	plot_hour_labels='0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'
	plot_hour_values=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	temp_plot_list=[]
	temp_plot_split=[]
	for i in log_entry_list:
		temp_plot_list=i.get_all()
		#print temp_plot_list
		temp_plot_split=temp_plot_list[4].split(':')
		plot_hour_values[int(temp_plot_split[0])]=plot_hour_values[int(temp_plot_split[0])]+1
	index = np.arange(24)
	plt.xticks(index)
	plt.plot(plot_hour_values)
	plt.ylabel('Requests')
	plt.xlabel('Hour(0-23):')
	#plt.plot(plot_hour_values,explode=explode,labels=pie_hour_labels,shadow=True)
	#plt.axis('equal')
	#plt.legend(title="tcnica")
	
	plt.title('Requests per hour')
	plt.show()
	
	
if option==1:
	temp_pie1_list=[]
	temp_pie1_split=[]
	pie1_site_labels=[]
	pie1_values=[]
	
	for i in log_entry_list:
		temp_pie1_list=i.get_all()
		temp_pie1_split=temp_pie1_list[6].split('/')
		count=-1
		flag=0
		for j in pie1_site_labels:
			count=count+1
			if  temp_pie1_split[0][1:]==j:
				flag=1
				pie1_values[count]=pie1_values[count]+1
				break
		if flag==0:
			count=count+1
			pie1_site_labels.append(temp_pie1_split[0][1:])
			pie1_values.append(1)
	
	#print pie1_site_labels	
	plt.pie(pie1_values,labels=pie1_site_labels,shadow=True, autopct='%1.4f%%', pctdistance=1.1, labeldistance=1.2)
	plt.axis('equal')
	plt.legend(title="Requests")
	plt.title('Request Types')
	plt.show()
		
		
if option==1:
	temp_bar1_list=[]
	temp_bar1_split=[]
	temp_bar1_split2=[]
	temp_bar1_split3=[]
	bar1_site_labels=[]
	bar1_values=[]
	label_string="size/file"+"\n"
	for i in log_entry_list:
		temp_bar1_list=i.get_all()
		
		
		temp_bar1_split=temp_bar1_list[6].split(' ')
		temp_bar1_split2=temp_bar1_split[1].split('?')
		count=-1
		flag=0
		
		for j in bar1_site_labels:
			count=count+1
			if  temp_bar1_split2[0]==j:
				flag=1
				#bar1_values[count]=bar1_values[count]+1
				break
		if flag==0:
			count=count+1
			bar1_site_labels.append(temp_bar1_split2[0])
			temp_bar1_split3=temp_bar1_split2[0].split("/")
			label_string=label_string+"\n"+str(count)+" "+temp_bar1_split3[len(temp_bar1_split3)-1]
			if temp_bar1_list[8].isdigit():
				bar1_values.append(int(temp_bar1_list[8]))
			else:
				bar1_values.append(0)
	#print bar1_site_labels
	
	
	n_groups = len(bar1_site_labels)

	
	
	fig, ax = plt.subplots()

	index = np.arange(n_groups)
	bar_width = 0.25

	opacity = 0.4
	
	
	
	
	
	rects1 = plt.bar(index, bar1_values, bar_width,
		         alpha=opacity,
		         color='b',
		         label=label_string)	
	plt.xlabel('Files:')
	plt.ylabel('Size(bytes):')
	plt.title('Filewise response sizes')
	plt.xticks(index + bar_width, index)#bar1_site_labels
	plt.legend(loc=2,prop={'size':6.5})

	plt.tight_layout()
	plt.show()


#FreakEnd

	
	
	
	
