SharpEye™ is a simple application for analyzing the contents of the weblog of a webserver (like an apache server.)
It is assumed that all entries are in the apache combined log format (see http://httpd.apache.org/docs/1.3/logs.html),
which is an extension of the W3C standard Common log format (see http://www.w3.org/Daemon/User/Config/Logging.html#common-logfile-format).

Entries in a weblog store information such as the remote host IP address, user authentications, HTTP requests etc.
While this data could be useful in entirety, it would be much more useful if the data is displayed in a more accessible way.
This is what SharpEye attempts to do.

The program was developed in Python 2.7.6.

Input is a simple text file containing the weblog entries for a single 24 hour period, from 00:00 hrs to 23:59 hrs.

The program parses the file, extracts individual entries and outputs the following:
-A text file containing a table(request_report.txt) of relevant information from all requests,
-A line graph that shows hourly traffic to the web server,
-A pie chart showing all the different requests incoming to server (useful for analyzing which website gets more traffic, which methods are majorly used etc) and
-A bar graph showing the response sizes for various possible files on the server (giving a sample of the file size).

LIBRARIES REQUIRED:
-numpy
-matplotlib

USAGE:
Program consists of two files, wlog1.py and log_entry_module.py
Name of input file is supplied using the command line.
	>> python wlog1.py <weblog file>
    eg. >> python wlog1.py weblog.txt
The text file will be created at the program location and other graphs will be output one after the other.
Graphs can be saved as image files if desired.